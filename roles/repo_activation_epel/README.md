Repo Activation for EPEL
======================

The role makes use of the following variables which can be customized, i.e. in group_vars:

 - repo_activation_epel_satellite_name: Name of the repo on the satellite server. Default: *epel_redhat-{{ ansible_distribution_major_version }}
 - repo_activation_epel_enable_powertools: If repository PowerTools on non RHEL systems and repository CodeReady on RHEL systems should be enabled. Default: false
 - repo_activation_epel_satellite_codeready_name: Name of the CodeReady repo on the satellite server. Default: \*codeready\*
 - repo_activation_epel_centos_powertools_baseurl: Baseurl of the PowerTools repo on CentOS systems. Default: http://centos.ethz.ch/$releasever/PowerTools/$basearch/os/
 - repo_activation_epel_disable_epel: If the epel repository should be disabled. Default: false
 - repo_activation_epel_epel_priority: Sets a priority for the EPEL repo. Default: "0". Feature only supported on RHEL systems
 - repo_activation_epel_original_repo_priority: Sets the priority for satellite default repository if they are enabled and priorits is not set. Default: "10"
 - repo_activation_epel_optional_repo_name: Enables the optional repo to install the priority package. Default: "rhel-7-server-optional-rpms"

The role can be implemented in other roles with the following task:
```
- name: "Include the repo_activation_epel role"
  include_role:
    name: "repo_activation_epel"
    public: yes
```

Supported OS family RedHat. Tested EPEL and PowerTools/CodeReady repositories on RedHat 7 / 8, AlmaLinux 8

Repository Naming Convention for Red Hat Satellite
--------------------------------------------------

By default, the role expects the EPEL repositories to be defined in the Satellite as follows (all names are case insensitive):

 1. Create a product called *epel*; assign the GPG key for EPEL (predefined)
 1. In this product, create the repositories *redhat-7* and *redhat-8*
 1. Optional: create an activation key, add the subscription for the product *epel* defined previsously, and **deactivate** all repositories. Use this key in addition to your usual Red Hat activation key. Alternatively, you can configure your key similarly. If you add the subscription to a host by hand, deactivate non matching repositories, they will be all enabled by default which may cause issues.
