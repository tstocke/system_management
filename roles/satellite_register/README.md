Satellite Client Registration
======================

Install necessary preconditions and register a RHEL system to the Satellite and keep system updated with latet agent packages.

Registration will be skipped if the system is alreay registered, except the variable *sat_register_force* is set to true.

The roles makes use of the following variables which should be customized, i.e. in group_vars:

 - sat_config: the Satellite server that you want to register to, options are production, test for RedHat systems. Default: production. Mandatory.
 - sat_organization: your organization name in Satellite (i.e. ID-CD or MATL). Mandatory.
 - sat_activationkey: an activation key which adds needed repos and sets host collection(s). Mandatory.
 - sat_register_force: set to true if a system is already registered.
 - sat_update_all: update all packages after registration, recommended i.e. if used on a cloned template. Defaults to false.
