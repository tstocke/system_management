Satellite Client Configuration
======================

The roles makes use of the following variables which should be customized, i.e. in group_vars:

 - sat_katello_restarter: cron job to restart Katello every night. Default: true.
 - sat_install_sshkey: SSH key of Satellite to enable remote commands, defaults to true.
 - sat_install_tracer: Tracer feature, see https://www.redhat.com/en/blog/introduction-tracer-feature-satellite
 - sat_initial_repositories: activates or updates the Satellite Tools repository

TBD: This should be changed to dynamically detect the OS flavor (server or workstation).


