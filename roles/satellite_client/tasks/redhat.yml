---
- name: Configure OS specific variables
  ansible.builtin.include_vars: '{{ ansible_os_family | lower }}{{ ansible_distribution_major_version }}.yml'
  ignore_errors: true

- name: Get current release configuration
  ansible.builtin.command: "subscription-manager release"
  register: releasemsg
  changed_when: false

- name: Test if host is subscribed to EUS
  ansible.builtin.set_fact:
    eus_version: "{{ releasemsg.stdout | regex_search('[0-9]\\.[0-9]') }}"

- name: Configure EUS specific variables
  ansible.builtin.include_vars: '{{ ansible_os_family | lower }}{{ ansible_distribution_major_version }}_EUS.yml'
  when: eus_version | length != 0

- name: Enable latest tools default repository
  community.general.rhsm_repository:
    name: "{{ item }}"
  loop: "{{ sat_initial_default_repositories }}"
  when: sat_initial_repositories == []

- name: Enable more initial repositories
  community.general.rhsm_repository:
    name: "{{ item }}"
  loop: "{{ sat_initial_repositories }}"
  when: sat_initial_repositories != []

- name: Disable obsolete tools default repositories
  community.general.rhsm_repository:
    name: "{{ item }}"
    state: disabled
  loop: "{{ sat_obsolete_default_repositories }}"
  failed_when: false
  when: sat_obsolete_repositories == []

- name: Disable obsolete tools repositories
  community.general.rhsm_repository:
    name: "{{ item }}"
    state: disabled
  loop: "{{ sat_obsolete_repositories }}"
  failed_when: false
  when: sat_obsolete_repositories != []

- name: Install/update Katello agent
  ansible.builtin.package:
    name: katello-agent
    state: latest
  when: sat_install_agent

- name: Remove Satellite 5 agent
  ansible.builtin.package:
    name: ["rhnsd", "rhn-client-tools"]
    state: absent
  when: sat_remove_rhnsd

- name: Cron job to restart Katello agent regularly
  ansible.builtin.copy:
    src: "files/goferd_rhel{{ ansible_distribution_major_version }}.cron"
    dest: /etc/cron.daily/goferd-restart
    mode: "0700"
  when: sat_katello_restarter

# Tracer can detect services which need a restart, or if a reboot
# is necessary after an update

- name: Install tracer integration
  ansible.builtin.package:
    name: katello-host-tools-tracer
    state: latest
  when: sat_install_tracer and not ansible_distribution_major_version | int == 6

- name: Install tracer reboot service
  ansible.builtin.template:
    src: tracer_boot.service
    dest: /etc/systemd/system/tracer_boot.service
    mode: 0644
  when: sat_install_tracer and not ansible_distribution_major_version | int == 6

# No daemon-reload needed, executes only after reboot
- name: Activate tracer reboot service
  ansible.builtin.systemd:
    name: tracer_boot
    enabled: true
  when: sat_install_tracer and not ansible_distribution_major_version | int == 6

- name: Install Satellite SSH key
  ansible.posix.authorized_key:
    user: root
    state: present
    key: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDAv6Tbjm4HA5BekhB9kr1T5WRgeq6ioUUb3VlWrTay4Z/ruFJ88yVmHm46/yaTbNqMtJaQUkyER9kMmOlGHZhkNjAqNswS3H8ltYcv84vccc/d89DGMoj5MLbGoiS1bEPicoM41Hk4p4oa662aNQ4HRlUBHzlLcnTvo1YRO5YjKu+PUukxmc8zWYco9nvg0giA5Z0AUwVDZpZb5yVokcC8p+BhVRspyx/lLBzZXgif933bmh3MIsk88PGF4Z3LVfefcZvm1TaYzC/syhMILVxcjNjGu0g8hTuMvCi/nrMcfSy9z9Ri56Qu/RExCfyuPLGPj1dpzECSaNlICeyOoflN foreman-proxy@id-sat-prd.ethz.ch"
  when: sat_install_sshkey

- name: Attach Additional Repositories
  community.general.rhsm_repository:
    name: "{{ item }}"
  loop: "{{ sat_add_extra_repositories }}"

- name: Remove Obsolete Repositories
  community.general.rhsm_repository:
    name: "{{ item }}"
  loop: "{{ sat_remove_extra_repositories }}"

# RHEL7.8 has buggy rhsm-checkscript
- name: set default rhsm_process_timeout
  community.general.ini_file:
    path: /etc/rhsm/rhsm.conf
    mode: 0644
    section: rhmsd
    option: processTimeout
    value: "{{ sat_rhsm_process_timeout }}"
