
[![pipeline status](https://gitlab.ethz.ch/ansible-community/collections/system_management/badges/master/pipeline.svg)](https://gitlab.ethz.ch/ansible-community/collections/system_management/-/commits/master) 
# Ansible Collection - ethz.system_management

## Satellite

Roles supporting the deployment, software sources and maintenance of systems.

 - `satellite_register`: (un)registration handling
 - `satellite_client`: maintain the katello agent, set connection properties
 - `repo_activation_epel`: manage EPEL and CodeReady/PowerTools extra repositories
 - `ansible_client`: client side Ansible execution, manage vault password, pull and cron jobs
 - `ansible_master`: install latest Ansible on a Ansible controller
 - `system_update_manager`: Exclude pagages, install OS updates, reboot system if necessary

Roles are tested, if applicable, on EL7/8, Debian 10 Buster and Ubuntu 20.04
